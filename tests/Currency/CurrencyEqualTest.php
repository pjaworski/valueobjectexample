<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use pjaworski\ValueObject\Currency;

class CurrencyEqualTest extends TestCase
{
    public function testCurrencyEqual()
    {
        $originalCurrency = 'PLN';
        $expectedResult = true;
        $currency1 = new Currency($originalCurrency);
        $currency2 = new Currency($originalCurrency);

        $result = $currency1->equals($currency2) && $currency2->equals($currency1) && $currency1 !== $currency2;
        $this->assertEquals($expectedResult, $result);
    }

    public function testCurrencyNotEqual()
    {
        $originalCurrency1 = 'PLN';
        $originalCurrency2 = 'EUR';
        $expectedResult = false;
        $currency1 = new Currency($originalCurrency1);
        $currency2 = new Currency($originalCurrency2);

        $result = ($currency1->equals($currency2) || $currency2->equals($currency1)) && $currency1 !== $currency2;
        $this->assertEquals($expectedResult, $result);
    }
}
