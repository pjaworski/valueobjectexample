<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use pjaworski\ValueObject\Currency;

class CurrencyTypeTest extends TestCase
{
    public function testCurrencyType()
    {
        $originalCurrency = 'PLN';
        $expectedResult = 'PLN';
        $currency = new Currency($originalCurrency);

        $result = $currency->getCurrency();
        $this->assertEquals($expectedResult, $result);
    }

    public function testCurrencyTypeLowercase()
    {
        $originalCurrency = 'Pln';
        $expectedResult = 'PLN';
        $currency = new Currency($originalCurrency);

        $result = $currency->getCurrency();
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testCurrencyTypeNonAscii()
    {
        $originalCurrency = 'aąć';
        new Currency($originalCurrency);
    }

    /**
     * @expectedException \TypeError
     */
    public function testCurrencyTypeNumber()
    {
        $originalCurrency = 100;
        new Currency($originalCurrency);
    }
}
