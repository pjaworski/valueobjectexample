<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use pjaworski\ValueObject\Currency;
use pjaworski\ValueObject\Money;

class MoneyEqualTest extends TestCase
{
    public function testMoneyEqual()
    {
        $originalAmount1 = 10.0;
        $originalAmount2 = 10.0;
        $originalCurrency = 'PLN';
        $expectedResult = true;

        $money1 = new Money($originalAmount1, new Currency($originalCurrency));
        $money2 = new Money($originalAmount2, new Currency($originalCurrency));
        $result = $money1->equals($money2) && $money2->equals($money1) && $money1 !== $money2;
        $this->assertEquals($expectedResult, $result);
    }

    public function testMoneyNotEqualAmount()
    {
        $originalAmount1 = 10.0;
        $originalAmount2 = 10.1;
        $originalCurrency = 'PLN';
        $expectedResult = false;

        $money1 = new Money($originalAmount1, new Currency($originalCurrency));
        $money2 = new Money($originalAmount2, new Currency($originalCurrency));
        $result = ($money1->equals($money2) || $money2->equals($money1)) && $money1 !== $money2;
        $this->assertEquals($expectedResult, $result);
    }

    public function testMoneyNotEqualCurrency()
    {
        $originalAmount = 10.0;
        $originalCurrency1 = 'PLN';
        $originalCurrency2 = 'EUR';
        $expectedResult = false;

        $money1 = new Money($originalAmount, new Currency($originalCurrency1));
        $money2 = new Money($originalAmount, new Currency($originalCurrency2));
        $result = ($money1->equals($money2) || $money2->equals($money1)) && $money1 !== $money2;
        $this->assertEquals($expectedResult, $result);
    }
}
