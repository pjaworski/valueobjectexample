<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use pjaworski\ValueObject\Currency;
use pjaworski\ValueObject\Money;

class MoneyOperationDivideTest extends TestCase
{
    public function testMoneyOperationDivide()
    {
        $originalAmount1 = 10.0;
        $originalDivider = 2.5;
        $originalCurrency = 'PLN';
        $expectedOperationResult = 4.0;
        $expectedResult = true;

        $money1 = new Money($originalAmount1, new Currency($originalCurrency));
        $operationResult = $money1->divide($originalDivider);

        $result = $operationResult->getAmount() === $expectedOperationResult && $operationResult !== $money1;
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testMoneyOperationDivideByZeroException()
    {
        $originalAmount1 = 10.0;
        $originalDivider = 0;
        $originalCurrency = 'PLN';

        $money1 = new Money($originalAmount1, new Currency($originalCurrency));
        $money1->divide($originalDivider);
    }

    /**
     * @expectedException \TypeError
     */
    public function testMoneyOperationDivideTypeException()
    {
        $originalAmount1 = 10.0;
        $originalDivider = '1';
        $originalCurrency = 'PLN';

        $money1 = new Money($originalAmount1, new Currency($originalCurrency));
        $money1->divide($originalDivider);
    }
}
