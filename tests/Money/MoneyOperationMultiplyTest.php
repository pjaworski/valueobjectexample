<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use pjaworski\ValueObject\Currency;
use pjaworski\ValueObject\Money;

class MoneyOperationMultiplyTest extends TestCase
{
    public function testMoneyOperationMultiply()
    {
        $originalAmount1 = 10.0;
        $originalMultiplier = 2.1;
        $originalCurrency = 'PLN';
        $expectedOperationResult = 21.0;
        $expectedResult = true;

        $money1 = new Money($originalAmount1, new Currency($originalCurrency));
        $operationResult = $money1->multiply($originalMultiplier);

        $result = $operationResult->getAmount() === $expectedOperationResult && $operationResult !== $money1;
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @expectedException \TypeError
     */
    public function testMoneyOperationMultiplyTypeException()
    {
        $originalAmount1 = 10.0;
        $originalMultiplier = '1';
        $originalCurrency = 'PLN';

        $money1 = new Money($originalAmount1, new Currency($originalCurrency));
        $money1->multiply($originalMultiplier);
    }
}
