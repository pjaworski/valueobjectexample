<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use pjaworski\ValueObject\Currency;
use pjaworski\ValueObject\Money;

class MoneyCurrencyTest extends TestCase
{
    public function testMoneyCurrency()
    {
        $originalAmount = 10.0;
        $originalCurrency = 'PLN';
        $expectedResult = 'PLN';

        $money = new Money($originalAmount, new Currency($originalCurrency));
        $result = $money->getCurrency()->getCurrency();
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testMoneyCurrencyObjectException()
    {
        $originalAmount = 10.0;
        $originalCurrency = 'zł';

        new Money($originalAmount, new Currency($originalCurrency));
    }

    /**
     * @expectedException \TypeError
     */
    public function testMoneyCurrencyNotObjectException()
    {
        $originalAmount = 10.0;
        $originalCurrency = 'PLN';

        new Money($originalAmount, $originalCurrency);
    }
}
