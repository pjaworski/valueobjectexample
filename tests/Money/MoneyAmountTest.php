<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use pjaworski\ValueObject\Currency;
use pjaworski\ValueObject\Money;

class MoneyAmountTest extends TestCase
{
    public function testMoneyAmount()
    {
        $originalAmount = 10.0;
        $originalCurrency = 'PLN';
        $expectedResult = 10.0;

        $money = new Money($originalAmount, new Currency($originalCurrency));
        $result = $money->getAmount();
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @expectedException \TypeError
     */
    public function testMoneyAmountTypeException()
    {
        $originalAmount = '10.0';
        $originalCurrency = 'PLN';

        new Money($originalAmount, new Currency($originalCurrency));
    }
}
