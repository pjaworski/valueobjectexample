<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use pjaworski\ValueObject\Currency;
use pjaworski\ValueObject\Money;

class MoneyOperationAddTest extends TestCase
{
    public function testMoneyOperationAdd()
    {
        $originalAmount1 = 10.0;
        $originalAmount2 = 1.0;
        $originalCurrency = 'PLN';
        $expectedOperationResult = 11.0;
        $expectedResult = true;

        $money1 = new Money($originalAmount1, new Currency($originalCurrency));
        $money2 = new Money($originalAmount2, new Currency($originalCurrency));
        $operationResult = $money1->add($money2);

        $result = $operationResult->getAmount() === $expectedOperationResult && $operationResult !== $money1;
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testMoneyOperationAddDifferentCurrenciesException()
    {
        $originalAmount1 = 10.0;
        $originalAmount2 = 1.0;
        $originalCurrency1 = 'PLN';
        $originalCurrency2 = 'EUR';

        $money1 = new Money($originalAmount1, new Currency($originalCurrency1));
        $money2 = new Money($originalAmount2, new Currency($originalCurrency2));
        $money1->add($money2);
    }

    /**
     * @expectedException \TypeError
     */
    public function testMoneyOperationAddTypeException()
    {
        $originalAmount1 = 10.0;
        $originalAmount2 = 1.0;
        $originalCurrency = 'PLN';

        $money1 = new Money($originalAmount1, new Currency($originalCurrency));
        $money1->add($originalAmount2);
    }
}
