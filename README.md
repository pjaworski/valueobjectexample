# pjaworski/valueobjectexample
## Table of contents
* [General info](#markdown-header-general-info)
* [Technologies](#markdown-header-technologies)
* [Setup](#markdown-header-setup)
* [Testing](#markdown-header-testing)

## General info
Example of simple ValueObject class implementation

## Technologies
Project is created with:
- PHP 7.1
- PHPUnit 7

## Setup
To run this project, install it locally using composer type in console
```
composer install
```

## Testing
Unit test are included, you cana run them by console command
```
vendor/bin/phpunit
```

If you wish more verbose info from testing try
```
vendor/bin/phpunit --testdox
```