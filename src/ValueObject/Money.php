<?php

namespace pjaworski\ValueObject;

use InvalidArgumentException;

final class Money
{
    /** @var float */
    private $amount;

    /** @var Currency */
    private $currency;

    public function __construct(float $amount, Currency $currency)
    {
        $this->amount = $amount;
        $this->currency = $currency;
    }

    public function add(Money $toAdd): Money
    {
        if (!$this->currency->equals($toAdd->getCurrency())) {
            throw new InvalidArgumentException('Currency must be the same for add operation');
        }

        if (0.0 === $toAdd->amount) {
            return $this;
        }

        return new Money($this->amount + $toAdd->getAmount(), $this->currency);
    }

    public function reduce(Money $toReduce): Money
    {
        if (!$this->currency->equals($toReduce->getCurrency())) {
            throw new InvalidArgumentException('Currency must be the same for reduce operation');
        }

        if (0.0 === $toReduce->amount) {
            return $this;
        }

        return new Money($this->amount - $toReduce->getAmount(), $this->currency);
    }

    public function multiply(float $multiplier): Money
    {
        if (1.0 === $multiplier) {
            return $this;
        }

        return new Money($this->amount * $multiplier, $this->currency);
    }

    public function divide(float $divider): Money
    {
        if (0.0 === $divider) {
            throw new InvalidArgumentException('Divider must be other then 0.0 for divide operation');
        }
        if (1.0 === $divider) {
            return $this;
        }

        return new Money($this->amount / $divider, $this->currency);
    }

    public function equals(Money $toCheck): bool
    {
        return $this->amount === $toCheck->getAmount() &&
            $this->currency->equals($toCheck->getCurrency());
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function getCurrency(): Currency
    {
        return $this->currency;
    }
}
