<?php

namespace pjaworski\ValueObject;

use InvalidArgumentException;

final class Currency
{
    /** @var string */
    private $currency;

    public function __construct(string $currency)
    {
        if (!preg_match('/^[A-Z]{3}$/i', $currency)) {
            throw new InvalidArgumentException('Currency has to be exactly 3 letters');
        }

        $this->currency = strtoupper($currency);
    }

    public function equals(Currency $toCheck): bool
    {
        return $this->currency === $toCheck->getCurrency();
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }
}
